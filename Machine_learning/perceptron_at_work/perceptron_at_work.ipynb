{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Perceptron algorithm at work"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will look in detail at the Perceptron algorithm for learning a linear classifier in the case of binary labels."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. The algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This first procedure, **evaluate_classifier**, takes as input the parameters of a linear classifier (`w,b`) as well as a data point (`x`) and returns the prediction of that classifier at `x`.\n",
    "\n",
    "The prediction is:\n",
    "* `1`  if `w.x+b > 0`\n",
    "* `0`  if `w.x+b = 0`\n",
    "* `-1` if `w.x+b < -1`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def evaluate_classifier(w,b,x):\n",
    "    if (np.dot(w,x) + b) > 0:\n",
    "        return 1\n",
    "    if (np.dot(w,x) + b) <= 0:\n",
    "        return -1\n",
    "    return 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the Perceptron training procedure. It is invoked as follows:\n",
    "* `w,b,converged = train_perceptron(x,y,n_iters)`\n",
    "\n",
    "where\n",
    "* `x`: n-by-d numpy array with n data points, each d-dimensional\n",
    "* `y`: n-dimensional numpy array with the labels (each 1 or -1)\n",
    "* `n_iters`: the training procedure will run through the data at most this many times (default: 100)\n",
    "* `w,b`: parameters for the final linear classifier\n",
    "* `converged`: flag (True/False) indicating whether the algorithm converged within the prescribed number of iterations\n",
    "\n",
    "If the data is not linearly separable, then the training procedure will not converge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train_perceptron(x,y,n_iters=100):\n",
    "    n,d = x.shape\n",
    "    w = np.zeros((d,))\n",
    "    b = 0\n",
    "    done = False\n",
    "    converged = True\n",
    "    iters = 0\n",
    "    np.random.seed(None)\n",
    "    while not(done):\n",
    "        done = True\n",
    "        I = np.random.permutation(n)\n",
    "        for i in range(n):\n",
    "            j = I[i]\n",
    "            if (evaluate_classifier(w,b,x[j,:]) != y[j]):\n",
    "                w = w + y[j] * x[j,:]\n",
    "                b = b + y[j]\n",
    "                done = False\n",
    "        iters = iters + 1\n",
    "        if iters > n_iters:\n",
    "            done = True\n",
    "            converged = False\n",
    "    if converged:\n",
    "        print(\"Perceptron algorithm: iterations until convergence: \", iters)\n",
    "    else:\n",
    "        print(\"Perceptron algorithm: did not converge within the specified number of iterations\")\n",
    "    return w, b, converged"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Experiments with the Perceptron"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start with standard includes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "matplotlib.rc('xtick', labelsize=14) \n",
    "matplotlib.rc('ytick', labelsize=14)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The directory containing this notebook should also contain the two-dimensional data files, `data_1.txt` and `data_2.txt`. These files contain one data point per line, along with a label, like:\n",
    "* `3 8 1` (meaning that point `x=(3,8)` has label `y=1`)\n",
    "\n",
    "The next procedure, **run_perceptron**, loads one of these data sets, learns a linear classifier using the Perceptron algorithm, and then displays the data as well as the boundary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def run_perceptron(datafile):\n",
    "    data = np.loadtxt(datafile)\n",
    "    n,d = data.shape\n",
    "    # Create training set x and labels y\n",
    "    x = data[:,0:2]\n",
    "    y = data[:,2]\n",
    "    # Run the Perceptron algorithm for at most 100 iterations\n",
    "    w,b,converged = train_perceptron(x,y,100)\n",
    "    # Determine the x1- and x2- limits of the plot\n",
    "    x1min = min(x[:,0]) - 1\n",
    "    x1max = max(x[:,0]) + 1\n",
    "    x2min = min(x[:,1]) - 1\n",
    "    x2max = max(x[:,1]) + 1\n",
    "    plt.xlim(x1min,x1max)\n",
    "    plt.ylim(x2min,x2max)\n",
    "    # Plot the data points\n",
    "    plt.plot(x[(y==1),0], x[(y==1),1], 'ro')\n",
    "    plt.plot(x[(y==-1),0], x[(y==-1),1], 'k^')\n",
    "    # Construct a grid of points at which to evaluate the classifier\n",
    "    if converged:\n",
    "        grid_spacing = 0.05\n",
    "        xx1, xx2 = np.meshgrid(np.arange(x1min, x1max, grid_spacing), np.arange(x2min, x2max, grid_spacing))\n",
    "        grid = np.c_[xx1.ravel(), xx2.ravel()]\n",
    "        Z = np.array([evaluate_classifier(w,b,pt) for pt in grid])\n",
    "        # Show the classifier's boundary using a color plot\n",
    "        Z = Z.reshape(xx1.shape)\n",
    "        plt.pcolormesh(xx1, xx2, Z, cmap=plt.cm.PRGn, vmin=-3, vmax=3)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's run this on `data_1.txt`. Try running it a few times; you should get slightly different outcomes, because of the randomization in the learning procedure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Perceptron algorithm: iterations until convergence:  11\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXUAAAD8CAYAAACINTRsAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAUEklEQVR4nO3df8zdZXnH8c8HyQAD1HR1UJb5Y2GAIotsNRMEikRnpkvGYjY2MtRt2G2VH26aIug2JmrwR0Cc4FZdBMkUEzdMmS7xRwSjGPWpMChgakLVBGpbVqSytNiWa3+cU3h4fPqc0z73+d7ne1/vV/Kkfb7n9HvdV398evd+Ts/liBAAoA2H1F4AAKAcQh0AGkKoA0BDCHUAaAihDgANObT2ApYtWxYveMELai8DAHpl/fr1j0TEc+derx7qS497jt5z6z8849ox955QaTUAMP1Ofe1Jsv2j+R7j+AUAGkKoA0BDpjLUt5yyUVtO2Vh7GQDQO1MZ6gCAgzPVoc5uHQAOzFSHusRRDAAciKkPdQDA+HoT6uzYAWC03oQ6AGC03oU6u3UA2L/ehToAYP96GeqcrwPA/HoZ6gCA+fU61NmtA8AzjRXqts+yvc72Q7bD9pvmPG7bV9p+2PZO27fbPnkiK56DoxgAeNq4O/UjJW2QdKmknfM8vkbS2yRdLOllkrZK+rLto0osEou3bftWXXjZBXpk+7Y0tbPVrVmbnqfHWKEeEV+MiCsi4nOSnpz9mG1LequkqyPiPyJig6Q3SjpK0vmlF7w/7NYX9onPfEx33bdeH7/lY2lqZ6tbszY9T48SZ+ovlHSspC/tuxAROyV9XdLpBe6PRdq2favWfeVWRYTWffk/O91Z1KqdrW7N2vTcbc+jlAj1Y4ffbplzfcusx57B9irbM7ZnHtu+o8AShgU5X5/XJz7zMT355OAfWE8++WSnO4tatbPVrVmbnrvteZQqr36JiLURsSIiVixZenSNJaSxb0exe89uSdLuPbs721nUqp2tbs3a9Nxtz+MoEeo/GX57zJzrx8x6rFPs1p82e0exT1c7i1q1s9WtWZueu6s7rhKhvkmD8H71vgu2D5d0pqQ7C9wfi3DP9+9+akexz+49u3XPA3c1Wztb3Zq16bm7uuNyRIx+kn2kpOOHn94p6WpJ6yRtj4gf275M0hWS/lzSRknvknSWpBMj4mcL3fuE3zw+PvKFaw6+gxGOufeEid0bAGo49bUnyfb6iFgx97Fxd+orJN01/DhC0j8Nv//u4eMfkHStpOslzUhaLul3RwV6FziKAZDJoeM8KSJul+QFHg9JVw4/AACV9Pq9X8bFSx0BZJEi1AEgi1Shzm4dQOtShToAtC5dqHO+DqBl6UJ9H4IdQIvShrrErh1Ae1KHOgC0hlAXRzEA2kGoA0BD0oX68lvv0MrTLtRrnn+uVp52oZbfeoek9s/XmSHZft2atel5eqQK9eW33qGT33G9jnhomxyhIx7appPfcf1Twd4yZki2X7dmbXqeHqlC/YQP3KxDdz7xjGuH7nxCJ3zg5qc+b3G3zgzJ9uvWrE3P0zP1SEoW6oc//MgBXW8FMyTbr1uzNj1Pz9QjKVmo7zpu2VjXWzpfZ4Zk+3Vr1qbnNmeU9sbGNRdozxGHPePaniMO08Y1F8z7/BaCnRmS7detWZueu6s7rrGGZLRi8x+ulDQ4Wz/84Ue067hl2rjmgqeut4gZku3XrVmbnrurO66xZpRO0qRnlJbCrFMA06LEjFIAQA8Q6mNq4XwdQPsIdQBoCKF+AFp6qSOANhHqANAQQv0gsGMHMK0I9UUg2AFMG0IdABpCqC8SRzEApgmhDgANIdQLYbcOYBoUCXXbz7J9le1NtncNv32P7VRvGAYAtZXaqV8m6S2SLpF0kqRLh59fXuj+xexvRmkJnK/PL9sMSeZ1ditjzwspFeqnS7otIm6LiB9GxDpJ6yT9TqH7F5F5RmlN2WZIMq+zWxl7XkipUP+GpFfaPkmSbL9Y0jmSvljo/kWMM6O0BHbrT8s2Q5J5nfRcW6lQf7+kmyXdb3u3pPsk3RQRN8z3ZNurbM/Ynnls+45CSxityxmlHMUMZJshybxOeq6tVKifJ+kNks6X9FvD76+2/ZfzPTki1kbEiohYsWTp0YWWMNq4M0pRRrYZkszrpOdpUCrUPyjpQxFxS0TcGxE3S7pGU/aF0gOdUVpC5t16thmSzOvsrm7N2llmlD5b0t451/Zqyl4Hn3FGaU3ZZkgyr7O7ujVrp5hRavtGSa+S9FcanKefKmmtpE9FxNsW+rF9mVFaAnNOAZSw0IzSUjv1iyVdJekGSb8iabOkj0t6d6H7AwDGUCTUI+Jnkt46/MB+bDllI7t1ABM1VWfeAIDFIdQ7xuvXAUwSoV4JwQ5gEgh1AGgIoV4RRzEASiPUAaAhhPoUYLcOoBRCHQAaQqhPCc7XAZSQLtQnOc5ummvXlG3cGKPdupWx54WkCvWa4+zGrd3ibj3buDFGu3UrY88LSRXqXY2zW2ztlo5iso0bY7QbPdeWKtS7HGc3TbVryjZujNFu9FxbqlCvOc7uYGr3fbeebdwYo93oeRqkCvUa4+ymoXYt2caNMdqtu7o1a2cZZ9cLNcfZHWztfbv1Pr4Pe7ZxY4x2665uzdopxtktRqZxdovRx1AHMBkLjbNLdfzSZ30/XwfQDUIdABpCqPdIS69fBzAZhHoPEewA9odQB4CGEOo9xVEMgPkQ6gDQEEK959ixA5iNUAeAhhDqjWC3DkAi1AGgKYR6QzhfB1As1G0vt32T7W22d9m+3/bk3/7wAGWYUUqwD2ScXUnP7dcdpUio236OpG9KsqTXSXqRpIslbS1x/1L6MKMU5WScXUnP7dcdpdROfY2kzRHxhoj4TkRsioivRsQDhe5fRF9mlJaQ/Sgm4+xKes7R8yilQv1cSd+2/VnbW23fbfsi257vybZX2Z6xPfPY9h2FljAaM0rzyDi7kp5z9DxKqVD/dUmrJT0o6TWSrpN0taS3zPfkiFgbESsiYsWSpUcXWsJofZtRWkLG3XrG2ZX0nKPncZQK9UMkfS8iLo+IuyLik5I+ov2Eei3MKM0h4+xKeu6udpYZpZsl3T/n2gOSLi10/yL6OKO0hD7POT0YGWdX0nN3tVPMKLX9aUm/FhFnzrp2laTXR8SLF/qxzCjtTpZQB1rXxYzSayW93PY7bR9v+48kXSLp+kL3RwEZz9eBbIqEekR8V4NXwPyxpA2S3ivp7yXdUOL+KCf7Sx2B1pU6U1dEfEHSF0rdDwBw4Hjvl6TYrQNtItQBoCGEemKcrwPtIdQBoCGEOtitAw0h1AGgIYQ6JHG+DrSCUMczEOxAvxHqANCQdKGeYUbpYuu2chSTcXYlPbdfd5RUoZ5xRuli6vY92DPOrqTn9uuOkirUM80orV23toyzK+k5R8+jpAr1jDNKF1u3r0cxGWdX0nOOnkdJFeoZZ5TW7LmWjLMr6TlHz+NIFeoZZ5SWqtun3XrG2ZX03F3tLDNKeyHjjNKSdfsy6zTj7Ep67q52ihmli8GM0v6Z9lAHWtfFjFIk0qejGCAbQh0AGkKo46D09aWOQOsIdQBoCKGORWG3DkwXQh0AGkKoY9E4XwemB6GOYgh3oD5CHQAaQqijOHbrQD2EOgA0ZCKhbvty22H7o5O4P6Yf5+tAHcVD3fbLJa2SdE/pe5fAjNJue86IeZ3dytjzQoqGuu0lkv5d0l9IerTkvUtgRmm3PUs5z9eZ19mtjD0vpPROfa2kz0XE1wrftwhmlHZXNyvmddJzbcVC3fabJR0v6V1jPHeV7RnbM49t31FqCSMxo7S7urNlOl9nXic911Yk1G2fKOl9ks6PiN2jnh8RayNiRUSsWLL06BJLGAszSrurO5/Wg515nfQ8DUrt1E+TtEzSfbb32N4jaaWk1cPPD1v4h3eDGaXd1c2IeZ3d1a1ZO8uM0s9Lmplz7ZOSfqDBDv7nheosCjNKu+15Pn2Zc3owmNfZXd2atdPOKLV9u6QNEXHRQs9jRmlOLYY60BVmlGLqtH6+DtRS6vjlF0TE2ZO6NwBgfuzUUU2mlzoCXSHUAaAhhDqqY7cOlEOoYypwFAOUQagDQEMIdUwVduvA4hDqANAQQh1Th/N14OAR6gDQkHShzji7/vTc1906o926lbHnhaQKdcbZ5ei5Nka7dStjzwtJFeqMs+uubqnafTtfZ7QbPdeWKtQZZ9dd3dK1+xLsjHaj59pShTrj7LqrW7t2DYx2o+dpkCrUGWfXXd1J1J72oxhGu3VXt2btLOPseoFxdjl6roXRbt3VrVk77Ti7cTHODgeDcXjIjHF2AJAEoY5emvbzdaAWQh0AGkKoo9fYrQPPRKij9ziKAZ5GqANAQwh1NIPdOkCoA0BTCHU0hfN1ZEeoA0BDCHU0iR07siLUAaAhRULd9uW2v2t7h+1ttm+z/ZIS9y6tr/M6+1i3dm0p3ytiMs7rzNjzQkrt1M+WdIOk0yWdI2mPpK/YXlro/kVknNeZsee5Mh3FZJzXmbHnhRQJ9Yh4TUR8MiI2RMS9ki6Q9FxJryhx/1L6Pq+zT3Vr184o47zOjD2PMqkz9aOG9350vgdtr7I9Y3vmse07JrSEX9TKvM4+1K1dez6t79YzzuvM2PMokwr16yTdLelb8z0YEWsjYkVErFiy9OgJLeEXZZzXmbHnjDLO68zY8ziKh7rtaySdIen1EbG39P0Xo6V5ndNet3bt/Wn1fD3jvM6MPY+j6IxS29dK+hNJr4yIB0veu4SM8zoz9pxRxnmdGXseR7EZpbavk3SeBoH+wLg/jhmlqIU5p+irhWaUFtmp275eg1e8nCvpUdvHDh96PCIeL1EDADBaqTP11Rq84uWrkjbP+nh7ofsDxbV6vo7ciuzUI8Il7gPUsOWUjRzFoBm89wsANIRQB8RRDNpBqANAQwh1YBZ26+g7Qh0AGkKoA3Nwvo4+I9QBoCGEOrAf7NbRR4Q6sACOYtA36UI947xOeu5+NmotGed1Zux5IalCPeO8TnouU7cvu/WM8zoz9ryQVKGecV4nPXdXt7aM8zoz9jxKqlDPOK+TnsvVnfbz9YzzOjP2PEqqUM84r5Oeu6tbU8Z5nRl7HkeqUM84r5Oey9edxt16xnmdGXseR9EZpdMu47xOep5M3X3BPi3vw55xXmfGnsdRbEbpwWJGKfpuWoIdeSw0ozTV8QsAtI5QBxZp2l8Vg1wIdQBoCKEOFMJuHdOAUAeAhhDqQEGcr6M2Qh0AGkKoAxPAbh21EOrAhHAUgxoIdQBoCKEOTBg7dnSpaKjbXm17k+1dttfbPrPk/QEACysW6rbPk3SdpPdJOlXSnZL+2/bzStUA+ozdOrpQcqf+d5JujIiPR8QDEXGxpM2S/qZgDQDAAoqEuu1fkvTbkr4056EvSTq9RA2gBZyvY9JKDclYJulZkrbMub5F0qvmPtn2Kkmrhp8+8XvP+4MNhdbRF8skTX5I6HTJ1nO2fiV67trz57tYZfJRRKyVtFaSbM/M90bvLaPn9mXrV6LnaVHqTP0RSXslHTPn+jGSflKoBgBghCKhHhE/l7Re0qvnPPRqDV4FAwDoQMnjl2sk3Wz7O5K+KemvJR0n6V9G/Li1BdfQF/Tcvmz9SvQ8FYoOnra9WtIaScslbZD0txHx9WIFAAALKhrqAIC6eO8XAGgIoQ4ADakW6pne/Mv25ba/a3uH7W22b7P9ktrr6tLw5yBsf7T2WibJ9nLbNw1/nXfZvt/2ytrrmhTbz7J91aw/y5tsv8d2lf8DMwm2z7K9zvZDw9/Db5rzuG1fafth2ztt32775ErLrRPqCd/862xJN2jwlgnnSNoj6Su2l9ZcVFdsv1yD/0F8T+21TJLt52jwyi9Lep2kF0m6WNLWmuuasMskvUXSJZJOknTp8PPLay6qsCM1eOHHpZJ2zvP4Gklv0+DX+mUa/Hp/2fZRna1wlipfKLX9bUn3RMSbZ137gaTPRURLvxnmZftISY9JOjcibqu9nkmyvUTS9yRdKOkfJW2IiIvqrmoybL9P0sqIeEXttXTF9n9J+t+IeOOsazdJ+uWI+P16K5sM249Luigibhx+bkkPS/poRLx3eO0IDYL97RHxr12vsfOdOm/+JUk6SoOf+0drL6QDazX4y/prtRfSgXMlfdv2Z21vtX237YuGf/Bb9Q1Jr7R9kiTZfrEG/xr9YtVVdeeFko7VrDyLiJ2Svq5KeVbj3OuA3vyrUddJulvSt2ovZJJsv1nS8ZL+rPZaOvLrklZLulbS1ZJeKumfh4+1+rWE92uwSbnf9l4NMuW9EXFD3WV15tjht/Pl2a92vBZJld7QKzPb10g6Q9IZEbG39nomxfaJGnzN5IyI2F17PR05RNLMrCPEu2z/hgZnzK2G+nmS3iDpfEn3afAX2XW2N0XEv1VdWVI1vlCa9s2/bF8r6U8lnRMRD9Zez4SdpsG/yu6zvcf2HkkrJa0efn5Y3eVNxGZJ98+59oCkVl8AIEkflPShiLglIu6NiJs1eMuQ5r82NrQvs6YmzzoP9axv/mX7Oj0d6N+vvZ4OfF7SKRrs3PZ9zEi6Zfj9n9db2sR8U9KJc66dIOlHFdbSlWdrsEmbba/y/B+YTRqE91N5ZvtwSWeqUp7VOn452Df/6iXb10u6QIMvpD1qe9853OMR8Xi9lU1ORPxU0k9nX7P9f5K2R0SrQ1GulXSn7XdK+qwGL9e9RNIVVVc1WbdJeoftTRocv5yqwWjLT1VdVUHDV6sdP/z0EEnPs/1SDX4v/9j2hyVdYfv7kjZKepekxyV9usqCI6LKhwZfUPqhpCc02LmfVWstHfQa+/m4svbaOv55uF2Dl35VX8sEe3ydpP+RtEuDP+CXaPjS4RY/NPgi6Yc1+NfITkkPavC1lMNrr61gj2fv58/vjcPHLelKDY7fdkm6Q9JLaq2XN/QCgIZkOfcCgBQIdQBoCKEOAA0h1AGgIYQ6ADSEUAeAhhDqANAQQh0AGvL/Vump0ksy9ocAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "run_perceptron('data_1.txt')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now, let's try running it on `data_2.txt`. *What's going on here?*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Perceptron algorithm: did not converge within the specified number of iterations\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXUAAAD8CAYAAACINTRsAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAASe0lEQVR4nO3df6xfdX3H8ee7ktneQHSODpxZ75Wwgb8SWeumiBSYbn+4P0jMRpTpMJt11ALbNDrAZWwVp9OAbEC2mm2A1ECyPwxMl6iLnVEI86JM+WFcVloSe5E68UcHtaN974/zrVwu39vea8/3nM/9nOcjObn9nu/9nvfnc7/tq6fnfPt5R2YiSarDqr4HIElqj6EuSRUx1CWpIoa6JFXEUJekihzX9wBOPPHEnJmZ6XsYkrSi3Hvvvd/NzLUL9/ce6jMzM8zOzvY9DElaUSJi97j9Xn6RpIoY6pJUEUNdkipiqEtSRQx1SaqIoS5JFTHUJakihrokVcRQl6SKGOqSVBFDXZIqYqhLUkUMdUmqiKEuSRVZUqhHxNkRcUdEfDsiMiIuWvB8RMRVEbEnIp6MiB0R8bKJjFiStKilnqkfD9wPXAY8Oeb59wLvBi4BXgU8BnwuIk5oY5Ct2r4dZmZg1arm6/bt9dd2zvXX7bO2c+52zkeTmcvagH3ARfMeBzAHXDlv3xrgR8A7j3a89evXZ2duvTVzaioTnt6mppr9tdZ2zvXX7bO2c+52zvMAszkuo8ftPNI2JtRPARJ41YLv+zRw89GO12moT08/8404vE1P11vbOddft8/azrnbOc+zWKhH89zSRcQ+YEtm3jR6fCbwZWA6Mx+Z933/CLwoM39zzDE2AZsA1q1bt3737rFdmdq3alXz43/2gODQoTprO+f66/ZZ2zl3V/dZ5eLezNywcH8vn37JzG2ZuSEzN6xd+6y+qZOzbt3y9tdQ2znXX7fP2s65u7pL1EaoPzr6etKC/SfNe64MV18NU1PP3Dc11eyvtbZzrr9un7Wdc3d1l2rcNZkjbSx+o/SKeftWAz+ktBulmc3NjOnpzIjma5c3N/qq7Zzrr9tnbefc+U3SzGO8ph4RxwOnjh7eBXwIuAP4XmY+EhHvA64A3g58C3g/cDZwWmb+6EjH3rBhQ87Ozi7vbyJJGrjFrqkft8TXbwC+MO/xX4y2m4GLgL+m+RjjDcDPAvcAv3G0QJcktWtJoZ6ZO2gusyz2fAJXjTZJUk9c+0WSKmKoS1JFDHVJqoihLkkVMdQlqSKGuiRVxFCXpIoY6pJUEUNdkipiqEtSRYYX6iX3FpS0MhScI0td0KsO27fDpk3wxBPN4927m8cAF17Y37gkrRyF58iy29m1rdOld2dmmjdgoelp2LWrmzFIWtkKyZGi2tn15pFHlrdfkhYqPEeGFeqF9xaUtAIUniPDCvXSewtKKl/hOTKsUL/wQti2rbn2FdF83batiJsbklaIwnNkWDdKJakS3iiVpAEw1CWpIoa6JFXEUJekihjqklQRQ12SKmKoS1JFDHVJqoihLkkVMdQlqSKGuiRVpJVQj4jnRMTWiHg4IvaPvn4gIobVWUmSetbWmfr7gHcBlwKnA5eNHl/e0vHbU3BvQUkrRME50taZ9JnAnZl55+jxroi4A/i1lo7fjsJ7C0paAQrPkbbO1L8EnBsRpwNExEuB84DPtHT8dlx55dNvxGFPPNHsl6SlKDxH2jpT/zBwAvBgRBwcHffqzLxx3DdHxCZgE8C6LltAFd5bUNIKUHiOtHWmfgHwNuAtwK+Mfr05In5/3Ddn5rbM3JCZG9auXdvSEJag8N6CklaAwnOkrVD/CPDRzLwtM7+RmZ8ArqG0G6WF9xaUtAIUniNthfoUcHDBvoMtHr8dhfcWlLQCFJ4jrfQojYibgNcD7wQeAM4AtgG3ZOa7j/Rae5RK0vIt1qO0rRullwBbgRuBnwfmgI8Df9nS8SVJS9BKqGfmj4A/Gm2SpJ6Udc1bknRMDHVJqoihLkkVMdQlqSKGuiRVxFCXpIoY6pJUEUNdkipiqEtSRYYX6gW3oarV3NwcGzdu5NFHH7VuxbUHpeQcycxet/Xr12dnbr01c2oqE57epqaa/ZqYiy++OFetWpWbN2+2bsW1B6OQHAFmc0ymtrJK47HodJXGmZmmn+BC09Owa1c3YxiYubk5TjnlFPbv38+aNWvYuXMnJ598snUrqz0oheTIYqs0DuvyS+FtqGq0detWDh06BMDBgwfZunWrdSusPSiF54hn6uCZ+oTMP3M8rIszyKHV7bv24BSSI56pQ/FtqGoz/8zxsC7OIIdWt+/ag1N4jgwr1AtvQ1Wbu+++mwMHDjxj34EDB7jrrrusW1HtwSk8R4Z1+UWSKuHlF0kaAENdkipiqEtSRQx1SaqIoS5JFTHUJakihrokVcRQl6SKGOqSVBFDXZIqYqhLUkUMdUmqSGuhHhEvjIibI2JvROyPiAcjYmNbx29Nyb0FKzW0XqH2KB2AknNkXI+75W7A84GdwC3ArwIvBn4deMnRXmuP0voNrVeoPUorV0iOMMkepRHxQWBjZr52ua+181HdhtYr1B6lA1BIjkx66d3zgXsi4vaIeCwi7ouILRERiwxmU0TMRsTs3r17WxrCEhTeW7BGQ+sVao/SASg9R8advi93A/aPtr8CzgDeDuwDthzttZ1efpmefuY/mQ5v09PdjWFA9uzZk6tXr07gJ9uaNWtybm7OuhXVHpxCcoRFLr+0daa+CvhqZl6emV/LzH8C/gZ4V0vHb0fhvQVrM7ReofYoHYjCc6StUJ8DHlyw7yFgXUvHb0fhvQVrM7ReofYoHYjCc6StG6WfBH4xM183b99W4E2Z+dIjvdYepZK0fJO+UXot8OqIuDIiTo2I3wYuBW5o6fiSpCVoJdQz8ys0n4D5HeB+4Grgz4Ab2zi+JGlpjmvrQJn5aeDTbR1PkrR8rv0iSRUx1CWpIoa6JFXEUJekihjqklQRQ12SKmKoS1JFDHVJqoihLkkVGV6ol9xbsFJD6xVqj9IBKDlHxi2y3uVmj9L6Da1XqD1KK1dIjjDJHqXHwh6ldRtar1B7lA5AITky6aV3V4bSewtWaGi9Qu1ROgCF54hn6uCZ+oTMP3M8rIszyKHV7bv24BSSI56pQ/G9BWsztF6h9igdiMJzZFihXnhvwdoMrVeoPUoHovAcGdblF0mqhJdfJGkADHVJqoihLkkVMdQlqSKGuiRVxFCXpIoY6pJUEUNdkipiqEtSRQx1SaqIoS5JFZlIqEfE5RGREXH9JI4vSRqv9VCPiFcDm4Cvt33sVpTcW7BSQ+sVao/SASg5R8b1uPtpN+B5wH8D5wI7gOuP9hp7lNZvaL1C7VFauUJyhEV6lLYd6rcDHx79urxQn55+5htxeJue7m4MA7Nnz55cvXp1ArlmzZqcm5uzboW1B6WQHFks1Fu7/BIR7wBOBd6/hO/dFBGzETG7d+/etoZwdIX3FqzR0HqF2qN0AErPkXFJv9wNOA3YC5w2b98OPFMftPlnjoe3Ls4gh1a379qDU0iOMOEz9dcAJwIPRMRTEfEUsBHYPHr83JbqHJvCewvWZmi9Qu1ROhCF50hbof4p4BXAK+dts8Bto18fWPylHSq8t2BthtYr1B6lA1F4jkysR2lE7ADuz8wtR/o+e5RK0vLZo1SSBuC4SR04M8+Z1LElSeN5pi5JFTHUJakihrokVcRQl6SKGOqSVBFDXZIqYqhLUkUMdUmqiKEuSRUZXqiX3IZK0spQcI5MbJmAIm3fDps2wRNPNI93724eQzErrEkqXOE5MrFVGpeq01UaZ2aaN2Ch6WnYtaubMUha2QrJEVdphPLbUEkqX+E5MqxQX7duefslaaHCc2RYoV54GypJK0DhOTKsUC+8DZWkFaDwHBnWjVJJqoQ3SiVpAAx1SaqIoS5JFTHUJakihrokVcRQl6SKGOqSVBFDXZIqYqhLUkUMdUmqiKEuSRUx1CWpIq2EekRcHhFfiYgfRsTeiLgzIl7exrFbV3BvQUkrRME50taZ+jnAjcCZwHnAU8DnI+IFLR2/HYd7C+7eDZlP9xYs6A2RVLjCc2QiS+9GxPHAD4DzM/POI32vPUolrSiF5EjXS++eMDr244sMZlNEzEbE7N69eyc0hDEK7y0oaQUoPEcmFerXAfcBd497MjO3ZeaGzNywdu3aCQ1hjMJ7C0paAQrPkdZDPSKuAc4C3pSZB9s+/jEpvLegpBWg8BxpNdQj4lrgzcB5mbmzzWO3ovDegpJWgMJzpLUbpRFxHXABcG5mPrTU19mjVJKWb7Ebpce1dPAbgLcC5wOPR8TJo6f2Zea+NmpIko6urcsvm2k+8fJvwNy87T0tHV+StAStnKlnZrRxHEnSsXHtF0mqiKEuSRUx1CWpIoa6JFXEUJekihjqklQRQ12SKmKoS1JFDHVJqsjwQr3P3oJ91XbO9dfts7ZzLqaVHQCZ2eu2fv367Mytt2ZOTWU2nQWbbWqq2V9rbedcf90+azvnbuc8DzCbYzJ1WKE+Pf3MN+LwNj1db23nXH/dPms7527nPM9ioT6RxtPL0el66qtWNT/+hSLg0KE6azvn+uv2Wds5d1f3WeW6bTxdpj57C/ZV2znXX7fP2s65u7pLNKxQ77O3YF+1nXP9dfus7Zy7q7tU467JdLl1ek09s7mZMT2dGdF87fLmRl+1nXP9dfus7Zw7v0ma6TV1SaqK19QlaQAMdUmqiKEuSRUx1CWpIoa6JFXEUJekihjqklQRQ12SKmKoS1JFDHVJqoihLkkVaTXUI2JzRDwcEfsj4t6IeF2bx5ckHVlroR4RFwDXAR8EzgDuAv41IspYZFiSBqDNM/U/AW7KzI9n5kOZeQkwB1zcYg1J0hG0EuoR8TPAeuCzC576LHBmGzUkSUd3XEvHORF4DvCdBfu/A7x+4TdHxCZg0+jhjyPi/pbGsVKcCHy370F0bGhzHtp8wTl3bXrczrZCfVkycxuwDSAiZsct9F4z51y/oc0XnHMp2rqm/l3gIHDSgv0nAY+2VEOSdBSthHpmHgDuBd6w4Kk30HwKRpLUgTYvv1wDfCIi/gP4MvCHwC8Af3eU121rcQwrhXOu39DmC865CK02no6IzcB7gRcC9wN/nJlfbK2AJOmIWg11SVK/XPtFkipiqEtSRXoL9SEt/hURl0fEVyLihxGxNyLujIiX9z2uLo1+BhkR1/c9lkmKiBdGxM2j93l/RDwYERv7HtekRMRzImLrvD/LD0fEByKil/8DMwkRcXZE3BER3x79Hr5owfMREVdFxJ6IeDIidkTEy3oabj+hPsDFv84BbqRZMuE84Cng8xHxgj4H1ZWIeDXN/yD+et9jmaSIeD7NJ78CeCPwEuAS4LE+xzVh7wPeBVwKnA5cNnp8eZ+DatnxNB/8uAx4cszz7wXeTfNev4rm/f5cRJzQ2Qjn6eVGaUTcA3w9M98xb99/Af+cmTX9ZhgrIo4HfgCcn5l39j2eSYqI5wFfBf4A+HPg/szc0u+oJiMiPghszMzX9j2WrkTEvwD/k5m/N2/fzcDPZeZv9TeyyYiIfcCWzLxp9DiAPcD1mXn1aN8ammB/T2b+fddj7PxM3cW/ADiB5mf/eN8D6cA2mr+sv9D3QDpwPnBPRNweEY9FxH0RsWX0B79WXwLOjYjTASLipTT/Gv1Mr6PqzouBk5mXZ5n5JPBFesqzPq57LWvxr0pdB9wH3N33QCYpIt4BnAr8bt9j6cgpwGbgWuBDwCuBvx09V+u9hA/TnKQ8GBEHaTLl6sy8sd9hdebk0ddxefaijscC9LSg15BFxDXAWcBZmXmw7/FMSkScRnPP5KzM/L++x9ORVcDsvEuIX4uIX6K5xlxrqF8AvA14C/AAzV9k10XEw5n5D72ObKD6uFE62MW/IuJa4M3AeZm5s+/xTNhraP5V9kBEPBURTwEbgc2jx8/td3gTMQc8uGDfQ0CtHwAA+Ajw0cy8LTO/kZmfoFkypPp7YyOHM6uYPOs81Ie6+FdEXMfTgf7NvsfTgU8Br6A5czu8zQK3jX59oL+hTcyXgdMW7PtlYHcPY+nKFM1J2nwHGc7/gXmYJrx/kmcRsRp4HT3lWV+XX37axb9WpIi4AXgrzY20xyPi8HW4fZm5r7+RTU5mfh/4/vx9EfG/wPcys9amKNcCd0XElcDtNB/XvRS4otdRTdadwJ9GxMM0l1/OoGlteUuvo2rR6NNqp44ergLWRcQraX4vPxIRHwOuiIhvAt8C3g/sAz7Zy4Azs5eN5obSLuDHNGfuZ/c1lg7mmotsV/U9to5/DjtoPvrV+1gmOMc3Av8J7Kf5A34po48O17jR3CT9GM2/Rp4EdtLcS1nd99hanOM5i/z5vWn0fABX0Vx+2w/8O/Dyvsbrgl6SVJGhXPeSpEEw1CWpIoa6JFXEUJekihjqklQRQ12SKmKoS1JFDHVJqsj/Azw2BFM7XJwZAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "run_perceptron('data_2.txt')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. For you to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"magenta\">Design a data set</font> with the following specifications:\n",
    "* there are just two data points, with labels -1 and 1\n",
    "* the two points are distinct, with coordinate values in the range [-1,1]\n",
    "* the Perceptron algorithm requires more than 1000 iterations to converge"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "12px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": false,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
