{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Winery classification using the one-dimensional Gaussian"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **Wine** data set is the running example for our discussion of the *generative approach to classification*. \n",
    "\n",
    "The data can be downloaded from the UCI repository (https://archive.ics.uci.edu/ml/datasets/wine). It contains 178 labeled data points, each corresponding to a bottle of wine:\n",
    "* The features (`x`): a 13-dimensional vector consisting of visual and chemical features for the bottle of wine\n",
    "* The label (`y`): the winery from which the bottle came (1,2,3)\n",
    "\n",
    "Before continuing, download the data set and place it in the same directory as this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Load in the data set"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start by loading the packages we will need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Standard includes\n",
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "# Useful module for dealing with the Gaussian density\n",
    "from scipy.stats import norm, multivariate_normal\n",
    "# installing packages for interactive graphs\n",
    "import ipywidgets as widgets\n",
    "from IPython.display import display\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual, IntSlider, Dropdown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we load the Wine data set. There are 178 data points, each with 13 features and a label (1,2,3).\n",
    "We will divide these into a training set of 130 points and a test set of 48 points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 'wine.data.txt' needs to be in the same directory\n",
    "data = np.loadtxt('wine.data.txt', delimiter=',')\n",
    "# Names of features\n",
    "featurenames = ['Alcohol', 'Malic acid', 'Ash', 'Alcalinity of ash','Magnesium', 'Total phenols', \n",
    "                'Flavanoids', 'Nonflavanoid phenols', 'Proanthocyanins', 'Color intensity', 'Hue', \n",
    "                'OD280/OD315 of diluted wines', 'Proline']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fix a particular \"random\" permutation of the data, and use these to effect the training / test split.\n",
    "We get four arrays:\n",
    "* `trainx`: 130x13, the training points\n",
    "* `trainy`: 130x1, labels of the training points\n",
    "* `testx`: 48x13, the test points\n",
    "* `testy`: 48x1, labels of the test points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Split 178 instances into training set (trainx, trainy) of size 130 and test set (testx, testy) of size 48\n",
    "# Also split apart data and labels\n",
    "np.random.seed(0)\n",
    "perm = np.random.permutation(178)\n",
    "trainx = data[perm[0:130], 1:14]       # the index 0 of data correspond to the class (sample vector y)\n",
    "trainy = data[perm[0:130], 0]          # this is exactly the first column of the data \n",
    "testx = data[perm[130:178], 1:14]     \n",
    "testy = data[perm[130:178], 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(130,)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(trainy==1).shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how many training points there are from each class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(43, 54, 33)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sum(trainy==1), sum(trainy==2), sum(trainy==3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{1.0: 43, 2.0: 54, 3.0: 33}"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dict(zip(np.unique(trainy, return_counts=True)[0], np.unique(trainy, return_counts=True)[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(43, 54, 33)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(trainy).count(1), list(trainy).count(2), list(trainy).count(3) # must use cell[41] which is faster"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(16, 17, 15)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sum(testy == 1), sum(testy==2), sum(testy==3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Look at the distribution of a single feature from one of the wineries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's pick just one feature: 'Alcohol'. This is the first feature, that is, number 0. Here is a *histogram* of this feature's values under class 1, along with the *Gaussian fit* to this distribution.\n",
    "\n",
    "<img src=\"histogram.png\">\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x, y):\n",
    "    return x+y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "2b8f890a83ea463ea866fe8b40eb4d71",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='x', options=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "interact_manual(f, x=widgets.Dropdown(options=range(20)), y=widgets.IntSlider(min=1, max=40, step=2));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "46947f4f418149e187e3fb4a4c14067d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='x', options=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact_manual(x=Dropdown(options=range(20)), y=IntSlider(value=8, min=1, max=40, step=2),z=widgets.Dropdown(options=range(20)))\n",
    "def g(x, y, z):\n",
    "    return x*y+z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hmm: how can we generate a figure like this? \n",
    "\n",
    "The following function, **density_plot**, does this for any feature and label. The first line adds an interactive component that lets you choose these parameters using sliders. \n",
    "\n",
    "<font color=\"magenta\">Try it out!</font> Jennifer!!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "8c641a5235564221b238585ad65c04ae",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(IntSlider(value=0, description='feature', max=12), IntSlider(value=1, description='label…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact_manual( feature=IntSlider(0,0,12), label=IntSlider(min=1,max=3, step=1))\n",
    "def density_plot(feature, label):\n",
    "    plt.hist(trainx[trainy==label,feature], density=True)\n",
    "    #\n",
    "    mu = np.mean(trainx[trainy==label,feature]) # mean\n",
    "    var = np.var(trainx[trainy==label,feature]) # variance\n",
    "    std = np.sqrt(var) # standard deviation\n",
    "    #\n",
    "    x_axis = np.linspace(mu - 3*std, mu + 3*std, 1000)\n",
    "    plt.plot(x_axis, norm.pdf(x_axis,mu,std), 'r', lw=2) # Normal probability density function\n",
    "    plt.title(\"Winery \"+str(label) )\n",
    "    plt.xlabel(featurenames[feature], fontsize=14, color='red')\n",
    "    plt.ylabel('Density', fontsize=14, color='red')\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([13.74, 13.56, 14.06, 14.2 , 13.76, 14.19, 13.83, 13.05, 13.24,\n",
       "       13.39, 14.22, 14.21, 14.83, 13.05, 13.5 , 13.73, 13.64, 14.3 ,\n",
       "       13.56, 13.71, 13.3 , 13.16, 13.24, 14.1 , 13.05, 13.94, 14.1 ,\n",
       "       14.75, 12.85, 14.06, 13.63, 13.82, 14.37, 14.39, 13.75, 14.38,\n",
       "       14.23, 14.38, 14.12, 13.48, 13.29, 13.41, 13.2 ])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "trainx[trainy==1, 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(130,)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(trainy==1).shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[9, 3, 4, 0, 9],\n",
       "       [1, 9, 1, 4, 1],\n",
       "       [8, 2, 4, 2, 5],\n",
       "       [9, 6, 2, 5, 5]])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.random.seed(6)\n",
    "A = np.random.randint(0, 10, size=(4, 5))\n",
    "A"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "B = np.array([1.4, 1.8, 3, 4, 5])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "std = np.zeros(13)\n",
    "for feature in range(0,13):\n",
    "    std[feature] = np.std(trainx[trainy==1,feature])\n",
    "np.argmin(std)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Fit a Gaussian to each class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's define a function that will fit a Gaussian generative model to the three classes, restricted to just a single feature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assumes y takes on values 1,2,3\n",
    "def fit_generative_model(x,y,feature):\n",
    "    k = 3 # number of classes\n",
    "    mu = np.zeros(k+1) # list of means\n",
    "    var = np.zeros(k+1) # list of variances\n",
    "    pi = np.zeros(k+1) # list of class weights\n",
    "    for label in range(1,k+1):\n",
    "        indices = (y==label)\n",
    "        mu[label] = np.mean(x[indices,feature])\n",
    "        var[label] = np.var(x[indices,feature])\n",
    "        pi[label] = float(sum(indices))/float(len(y))\n",
    "    return mu, var, pi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0.33076923 0.41538462 0.25384615]\n",
      "[13.78534884 12.31092593 13.15969697]\n",
      "[0.23325279 0.2819047  0.2851787 ]\n"
     ]
    }
   ],
   "source": [
    "feature = 0 # 'alcohol'\n",
    "mu, var, pi = fit_generative_model(trainx, trainy, feature)\n",
    "print(pi[1:])\n",
    "print(mu[1:])\n",
    "print(var[1:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, display the Gaussian distribution for each of the three classes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "e93c043286964fe2a4abd981fdf7e0ad",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(IntSlider(value=0, description='feature', max=12), Button(description='Run Interact', st…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact_manual( feature=IntSlider(0,0,12) )\n",
    "def show_densities(feature):\n",
    "    mu, var, pi = fit_generative_model(trainx, trainy, feature)\n",
    "    colors = ['r', 'k', 'g']\n",
    "    for label in range(1,4):\n",
    "        m = mu[label]\n",
    "        s = np.sqrt(var[label])\n",
    "        x_axis = np.linspace(m - 3*s, m+3*s, 1000)\n",
    "        plt.plot(x_axis, norm.pdf(x_axis,m,s), colors[label-1], label=\"class \" + str(label))\n",
    "    plt.xlabel(featurenames[feature], fontsize=14, color='red')\n",
    "    plt.ylabel('Density', fontsize=14, color='red')\n",
    "    plt.legend()\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Predict labels for the test set"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How well can we predict the class (1,2,3) based just on one feature? The code below lets us find this out."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c74f7e87065a4da18e8908445660a767",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(IntSlider(value=0, description='feature', max=12), Output()), _dom_classes=('widget-inte…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact( feature=IntSlider(0,0,12) )\n",
    "def test_model(feature):\n",
    "    mu, var, pi = fit_generative_model(trainx, trainy, feature)\n",
    "\n",
    "    k = 3 # Labels 1,2,...,k\n",
    "    n_test = len(testy) # Number of test points\n",
    "    score = np.zeros((n_test,k+1))\n",
    "    for i in range(0,n_test):\n",
    "        for label in range(1,k+1):\n",
    "            score[i,label] = np.log(pi[label]) + \\\n",
    "            norm.logpdf(testx[i,feature], mu[label], np.sqrt(var[label]))\n",
    "    predictions = np.argmax(score[:,1:4], axis=1) + 1\n",
    "    # Finally, tally up score\n",
    "    errors = np.sum(predictions != testy)\n",
    "    print(\"Test error using feature \" + featurenames[feature] + \": \" + str(errors) + \"/\" + str(n_test))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[5, 0, 3, 3],\n",
       "       [7, 9, 3, 5],\n",
       "       [2, 4, 7, 6]])"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.random.seed(0)\n",
    "A = np.random.randint(0,10, size=(3,4))\n",
    "A"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1, 2, 0])"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.argmin(A, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.1"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "12px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": false,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
