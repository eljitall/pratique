#include <stdio.h>

int main()
{
	int acker(int , int ) ;
	int m , n;
	printf ("Donner deux entiers positifs separes par un espace: ") ;
	scanf ("%d %d", &m, &n) ;
	printf ("acker (%d, %d) = %d\n", m, n, acker(m, n)) ;
}
int acker (int m, int n)
{
	if ( (m<0) || (n<0) )
		return(0) ;
	else if ( m==0 )
		return n+1;
	else if ( n==0 )
		return ( acker(m-1, 1) ) ;
	else
		return ( acker( m-1, acker(m, n-1)) ) ;
}
