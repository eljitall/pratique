/* LES VARIABLES LOCALES STATIQUES */
#include <stdio.h>
int i;  // declaration d'une variable globale. Elle sera toujours initialisee a 0. Cette variable n'a aucun rapport avec la variable statique i de la fonction fct

int main()
{
	void fct(void) ; //declaration de la fonction fct
	int n ;
	for ( n=1 ; n<=5 ; n++ )
		fct() ; //appel de la fonction fct que sera definie plutard.
	printf ("La valeur de la variable globale est initialisee a : %d\n", i) ;
}
void fct(void)
{
	static int i ; // declaration d'une variable static, sa valeur est modifiee a chaque appel de la fonction sa valeur est initialisee a 0.
	i++ ;
	printf ("appel numero : %d\n", i) ;
}
