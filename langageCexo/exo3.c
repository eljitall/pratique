#include <stdio.h>

void compte(void)
{
	static long n ;
	static long limit=1 ;
	n++ ;
	if ( n >= limit )
	{
		printf ("*** appel %ld fois ***\n", limit);
		limit *= 10 ;
	}
}

int main()
{
	void compte(void) ;
	long nmax = 100000 ;
	long i ;
	for ( i=1 ; i<= nmax ; i++ )  compte() ;
}
