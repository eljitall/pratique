#include <stdio.h>

int main()
{
	int a, b, c, i, Nterm;
	a = 1, b = 1 , c = 1;
	printf ("Donnez un entier positif: ");
	scanf ("%d", &Nterm) ;

	if ( Nterm == 1 || Nterm == 2)
		printf ("Le %d-ieme terme de la suite de Fibonacci est: %d\n", Nterm, a );
	 else
	 {
		 if ( Nterm <= 0)
			 printf ("l'entier doit etre positif.\n");
	 		else
			{
				for ( i=2 ; i<Nterm ; i++)
				{
					a = b ;
					b = c ;
					c = a + b ;
				}
				printf ("Le %d-ieme terme de la suite de Fibonacci est: %d\n", Nterm, c);
			}
	 }
}
