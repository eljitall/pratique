#include <stdio.h>

int main()
{
	int i ;
	float note, som ;
	i = 0, som = 0. ;
	while ( 1 )
	{ printf ("note %d : ", i+1) ;
	  scanf ("%f", &note) ;
	  if ( note < 0) break;
	  som += note ;
	  i++ ;
	}
	if ( i == 0 )
		printf (" Rien a faire\n") ;
	else
		printf ("moyenne de ces %d notes : %.2f\n", i, som/i);
}
