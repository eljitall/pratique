#include <stdio.h>

int main()
{
	int j ;
	float nb1, nb2 ;

	printf ("Choisissez l'operation que vous voudriez effectuer:\n");
	printf ("1 pour l'addition (+)\n") ;
	printf ("2 pour la soustraction (-)\n") ;
	printf ("3 pour la multiplication (*)\n") ;
	printf ("4 pour la division (/)\n") ;
	printf ("Votre choix: ") ;
	scanf ("%d", &j) ;
	switch (j)
	{
		case 1 : printf ("Vous avez choisi l'addition.\n") ;
			 printf ("Donner votre premiere operande: ") ;
			 scanf ("%f", &nb1) ;
			 printf ("Donner votre seconde operande: ") ;
			 scanf ("%f", &nb2) ;
			 printf ("%.2f + %.2f = %.2f\n", nb1, nb2, nb1 + nb2) ;
			 break ;
		case 2 : printf ("Vous avez choisi la soustraction.\n") ;
			 printf ("Donner votre premiere operande: ") ;
			 scanf ("%f", &nb1) ;
			 printf ("Donner votre seconde operande: ") ;
			 scanf ("%f", &nb2) ;
			 printf ("%.2f - %.2f = %.2f\n", nb1, nb2, nb1 - nb2) ;
			 break ;
		case 3 : printf ("Vous avez choisi la multiplication.\n") ;
			 printf ("Donner votre premiere operande: ") ;
			 scanf ("%f", &nb1) ;
			 printf ("Donner votre seconde operande: ") ;
			 scanf ("%f", &nb2) ;
			 printf ("%.2f * %.2f = %2.f\n", nb1, nb2, nb1 * nb2) ;
			 break ;
		case 4 : printf ("Vous avez choisi la division.\n") ;
			 printf ("Donner votre premiere operande: ") ;
			 scanf ("%f", &nb1) ;
			 printf ("Donner votre seconde operande: ") ;
			 scanf ("%f", &nb2) ;
			 if ( nb2 == 0 )   printf ("Tu veux detruire le monde, booooooommmm\n") ;
			     else
			 	printf ("%.2f / %.2f = %.2f\n", nb1, nb2, nb1 / nb2) ;
			 break ;
		default : printf ("Mauvais choix.\n") ;
	}
}
